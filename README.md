# XEP-0227 to Jabberd2 database Conversion

A script to convert [XEP-0227](http://xmpp.org/extensions/xep-0227.html)
data to [Jabberd2](http://jabberd2.org) data files.

Currently it works only with SQLite3 files … that’s what I need, but
certainly merge requests for MySQL, PostrgreSQL, or even BerkeleyDB
(does anybody still uses it?) are very welcome.
