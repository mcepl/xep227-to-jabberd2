#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
import logging
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.INFO)
import os.path
import sqlite3
import argparse
from xml.etree import ElementTree as ET


XEP_NS = "http://www.xmpp.org/extensions/xep-0227.html#ns"
ROSTER_NS = "jabber:iq:roster"


class Database(object):
    def __init__(self, filename, schemafile):
        if os.path.exists(filename):
            logging.info('Destroying existing database file %s', filename)
            os.remove(filename)
        self.db = sqlite3.connect(filename)
        logging.debug('db = %s', self.db)
        self.cur = self.db.cursor()
        logging.debug('cur = %s', self.cur)

        # Initialize schema
        self.cur.executescript(open(schemafile).read())

    def insert_values(self, table, names, columns):
        logging.debug('table = %s', table)
        logging.debug('names = %s', names)
        logging.debug('columns = %s', columns)
        question_marks = ", ".join('?' for x in names.split(', '))
        self.cur.execute('INSERT INTO %s (%s) VALUES (%s)'
                         % (table, names, question_marks), columns)
        self.db.commit()


class Roster(object):
    def __init__(self, el, parent):
        """
        roster-groups

        Stores user roster items only for those roster items that have
        an assigned group.

        Used by: mod_roster

         collection-owner -  owner JID
         object-sequence  -  auto increment number
         jid              -  JID contact person
         group            -  group which belongs to

        example

         mysql> select * from `roster-groups`;
         +-----------------+-----------------+----------------------+---------+
         | collection-owner| object-sequence | jid                  | group   |
         +-----------------+-----------------+----------------------+---------+
         | me@there.org    |            5461 | alabama@holiday.pl   | Friend  |
         | me@there.org    |            5460 | goodworker@athome.cz | Workers |
         | neo@outside.org |            5462 | alabama@holiday.pl   | Friend  |
         | neo@outside.org |            5463 | goodworker@athome.cz | Workers |

        roster-items

        Stores user roster items, including authorization status.

        Used by: mod_roster

         collection-owner -  owner JID
         object-sequence  -  auto increment number
         jid              -  JID contact person
         name             -  name of the person
         to               - authorization to
         from             - authorization from
         ask              - ask authorization

        example

         mysql> select * from `roster-items`;
         +------------------+-----------------+-----------------------+
         | collection-owner | object-sequence | jid                   |
         +------------------+-----------------+-----------------------+
         | neo@example.org |            5704 | bunny@matrix.cz        |
         | neo@example.org |            5712 | orfeus@dot.com         |
         | neo@example.org |            5711 | andrea@example.org     |

         +-----------------------+------+------+------+
         | name                  | to   | from | ask  |
         +-----------------------+------+------+------+
         |  Jitka Mala           |    1 |    1 |    0 |
         |  Ivan Mladek          |    1 |    1 |    0 |
         |  Andrea Farna         |    1 |    1 |    0 |
        """
        self.element = el
        self.db = parent.db
        self.parent = parent
        self.items = []
        self._parse()

    def _parse(self):
        logging.debug('self.element:\n%s', ET.tostring(self.element))
        for it in self.element.findall('item'):
            logging.debug('it = %s', ET.tostring(it))
            item = it.attrib.copy()
            group_els = it.findall('group')
            if group_els is not None:
                item['groups'] = [x.text for x in group_els]
            self.items.append(item)

    def add_to_database(self):
        owner = '%s@%s' % (self.parent.name, self.parent.parent.host_jid)
        logging.debug('owner = %s', owner)
        for it in self.items:
            logging.debug('it = %s', it)
            subs_str = it['subscription'].lower()
            to_sub = subs_str in ['to', 'both']
            from_sub = subs_str in ['from', 'both']
            ask = it.get('ask', 0)
            name = it.get('name', None)

            self.db.insert_values(
                '[roster-items]',
                '[collection-owner], jid, name, [to], [from], ask',
                (owner, it['jid'], name, to_sub, from_sub, ask))
            logging.debug('groups = %s', it['groups'])
            for grp in it['groups']:
                logging.debug('grp = %s', it['groups'])
                self.db.insert_values(
                    '[roster-items]',
                    '[collection-owner], jid, group', (owner, it['jid'], grp))


class User(object):
    def __init__(self, el, parent):
        """
        c2s authentication/registration table. Contains
        authentication information, including username, realm and
        password.

         username - login name
         realm    - domain
         password - clear text password

        example

         mysql> select * from authreg;
         +----------+------------------+----------+
         | username | realm            | password |
         +----------+------------------+----------+
         | someone  | example.org      | secret   |
         +----------+------------------+----------+
        """
        self.element = el
        self.db = parent.db
        self.parent = parent
        self.name = ''
        self.password = ''
        self.roster = None
        self._parse()

    def _parse(self):
        # TODO should we do this replace?
        # passw = attrs.getValueByQName("password").replace("\"", "\\\"")
        logging.debug('el:\n%s', ET.tostring(self.element))
        self.password = self.element.attrib['password']
        self.name = self.element.attrib['name']
        r_el = self.element.find('{%s}query' % ROSTER_NS)
        if r_el is not None:
            self.roster = Roster(r_el, self)

    def add_to_database(self):
        logging.debug('username, realm, password = %s',
                      (self.name, self.parent.host_jid, self.password))
        self.db.insert_values(
            'authreg', 'username, realm, password',
            (self.name, self.parent.host_jid, self.password))
        if self.roster is not None:
            self.roster.add_to_database()


class Host(object):
    def __init__(self, XML_element, parent):
        """
        """
        self.element = XML_element
        self.host_jid = XML_element.attrib['jid']
        self.users = []
        self.parent = parent
        self.db = self.parent.db
        self._parse()

    def _parse(self):
        for us_el in self.element.findall('user'):
            self.users.append(User(us_el, self))

    def add_to_database(self):
        for us in self.users:
            logging.debug('us = %s', us)
            us.add_to_database()


class XEP0227Parser(object):
    def __init__(self, datafile, dbfile, schemafile):
        self.hosts = []
        self.datafile = datafile
        self.file_parsed = ET.parse(self.datafile)
        self.tree = self.file_parsed.getroot()
        self.db = Database(dbfile, schemafile)
        self._parse()

    def _parse(self):
        logging.debug('self.tree = %s', self.tree)
        for host_el in self.tree.findall('host'):
            logging.debug('host_el = %s', host_el)
            host = Host(host_el, self)
            self.hosts.append(host)

    def add_to_database(self):
        for host in self.hosts:
            logging.debug('host = %s', host)
            host.add_to_database()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('xep0227_file', help='XEP-0227 format file')
    parser.add_argument('db_file', help='Jabberd2 database SQLite file')
    parser.add_argument('db_schema_file', help='Jabberd2 schema file')
    args = parser.parse_args()
    logging.debug('args = %s', args)

    processor = XEP0227Parser(os.path.expanduser(args.xep0227_file),
                              os.path.expanduser(args.db_file),
                              os.path.expanduser(args.db_schema_file))
    processor.add_to_database()
